bauble (0.9.7-3) UNRELEASED; urgency=medium

  * Team upload.
  * Take over into Debian Science team
    Closes: #903644
  * Fix watch file
  * Remove files depending on python-gdata manually since upstream patch
      https://github.com/Bauble/bauble.classic/commit/8f0e75861b7cc4cf
    does not apply cleanly
    Closes: #899007

 -- Andreas Tille <tille@debian.org>  Sun, 17 Mar 2019 18:34:09 +0100

bauble (0.9.7-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Move from deprecated pysupport to dh_python2. (Closes: #785949)

 -- Martin Pitt <mpitt@debian.org>  Tue, 18 Aug 2015 17:44:55 +0200

bauble (0.9.7-2) unstable; urgency=low

  * manpages moved near the binaries
  * remove string exceptions, invalid in python 2.6 (Closes: #585210)
  * support also bsd flavours (Closes: #588849)
  * updated to policy 3.9.1
  * support sqlalchemy 0.6

 -- Giacomo Catenazzi <cate@debian.org>  Mon, 02 Aug 2010 19:36:01 +0200

bauble (0.9.7-1) unstable; urgency=low

  * ack NMU of Jonathan Wiltshire: thanks! (Closes: #547809)
  * new upstream version (Closes: #551296)
  * don't install pyparsing module, but use debian version (Closes: #555366)

 -- Giacomo Catenazzi <cate@debian.org>  Wed, 16 Dec 2009 07:50:50 +0100

bauble (0.9.3-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Invoke setup.py to install files in correct locations (Closes: #547809).

 -- Jonathan Wiltshire <debian@jwiltshire.org.uk>  Wed, 18 Nov 2009 10:11:51 +0000

bauble (0.9.3-1) unstable; urgency=low

  [ Giacomo Catenazzi ]
  * New upstream release

 -- Giacomo Catenazzi <cate@debian.org>  Fri, 07 Aug 2009 12:59:37 +0200

bauble (0.9.2-1) unstable; urgency=low

  [ Giacomo Catenazzi ]
  * New upstream release

  [ Luca Falavigna ]
  * debian/control:
    - Minimum python version should be 2.5.
    - Remove useless B-D.
    - Add XS-Python-Version field.
    - Remove python from Depends, ${python:Depends} will provide it.

 -- Giacomo Catenazzi <cate@debian.org>  Sun, 26 Jul 2009 20:19:55 +0200

bauble (0.9.0~b5-3) unstable; urgency=low

  * ooops: architecture "all"
  * remove dh_desktop

 -- Giacomo Catenazzi <cate@debian.org>  Tue, 19 May 2009 22:11:32 +0200

bauble (0.9.0~b5-2) unstable; urgency=low

  * remove libgdk-pixbuf2 dependency: not needed

 -- Giacomo Catenazzi <cate@debian.org>  Tue, 19 May 2009 08:04:24 +0200

bauble (0.9.0~b5-1) unstable; urgency=low

  * Initial release (Closes: #507529)
  * Created bauble.1 and bauble-admin.1 manpages
  * install also icon bauble.xpm (but in usr/share/pixmaps)
  * Patched setup.cfg: don't try to download dependencies from net

 -- Giacomo Catenazzi <cate@debian.org>  Thu, 02 Apr 2009 13:02:01 +0200
